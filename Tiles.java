enum Tile {
	BLANK,
	HIDDEN_WALL,
	WALL,
	CASTLE
}

public class Tiles {
	String name;
	Tile tile;
	
	public Tiles(Tile tile) {
		this.tile = tile;
		if (tile == Tile.values() [0])
			this.name = "_";
		if (tile == Tile.values() [1])
			this.name = "_";
		if (tile == Tile.values() [2])
			this.name = "W";
		if (tile == Tile.values() [3])
			this.name = "C";
	}
	
	public String getName() {
		return this.name;
	}
	
	public String toString() {
		return this.name;
	}
	
}