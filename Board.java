import java.util.Random;

class Board {
	private Tiles[][] grid;
	final int size = 5;
	
	public Board() {
		this.grid = new Tiles[size][size];
		Random rand = new Random();
		
		for (int i = 0; i < grid.length; i++){
			int y = rand.nextInt(grid[i].length);
			for (int j = 0; j < grid[i].length; j++){
				grid[i][j] = new Tiles(Tile.BLANK);
				
				grid[i][y] = new Tiles(Tile.HIDDEN_WALL);
			}
		}
	}
	
	public String toString(){
		String board = "";
		for (Tiles[] s : grid){
			for (Tiles ti : s){
				board += ti + " ";
			}
		board += "\n";
		}
		return board;
	}
	
	public int placeToken(int row, int col){
		if (row < 0 || row >= 5 || col < 0 || col >= 5)
			return -2;
		
		if (grid[row][col].tile == Tile.CASTLE || grid[row][col].tile == Tile.WALL)
			return -1;
		
		if (grid[row][col].tile == Tile.HIDDEN_WALL){
			grid[row][col] = new Tiles(Tile.WALL);
			return 1;
		}
		
		if (grid[row][col].tile == Tile.BLANK){
			grid[row][col] = new Tiles(Tile.CASTLE);
			return 0;
		}
		
		return 0;
	}
}